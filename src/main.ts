import {User} from "./User";

const user = new User("John");
user.greet();

const user2 = new User("Jeff");
user2.greet();

user.shake_hands(user2);
user2.shake_hands(user);
