export class User {
    constructor(public name: String) {}

    greet(): void {
        console.log(`Hi! My name is ${this.name}.`);
    }

    shake_hands(other: User): void {
        console.log(`${this.name} shaked ${other.name}'s hands.`);
    }
}
